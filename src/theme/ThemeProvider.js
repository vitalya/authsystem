import React, {createContext, useContext, useState} from 'react';
import {ThemeProvider as StyledThemeProvider} from 'styled-components';

const ThemeContext = createContext();
export const useTheme = () => useContext(ThemeContext);

const themes = {
    light: {
        background: '#FFF',
        color: '#000',
    },
    dark: {
        background: '#131A22',
        color: '#FFF',
    }
};


export const ThemeProvider = ({children}) => {
    const [theme, setTheme] = useState('light');  // Default theme is light

    const toggleTheme = (newTheme) => {
        setTheme(newTheme);
    };

    return (
        <ThemeContext.Provider value={{theme, toggleTheme}}>
            <StyledThemeProvider theme={themes[theme]}>
                {children}
            </StyledThemeProvider>
        </ThemeContext.Provider>
    );
};
