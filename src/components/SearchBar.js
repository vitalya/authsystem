import React, { useEffect, useState } from 'react';
import styled, { keyframes } from 'styled-components';

const SearchContainer = styled.div`
    display: flex;
    align-items: center;
    width: 100%; /* Здесь можно задать нужную ширину */
    max-width: 70%; /* Максимальная ширина, чтобы строка не стала слишком широкой */
    height: 40px;
    border-radius: 20px;
    background-color: #f2f2f2;
    margin-top: 40px;
    padding: 5px 10px;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.5);
    cursor: text; /* Чтобы курсор менялся на текстовый при нажатии */
`;

const SearchInput = styled.input`
    flex: 1;
    border: none;
    background: none;
    outline: none;
    padding-left: 10px;
    width: calc(100% - 20px);
`;

const SearchIcon = styled.i`
    margin-right: 5px;
    color: #aaa;
`;

const SearchBar = ({ phrases }) => {
    const [query, setQuery] = useState('');
    const [currentPhraseIndex, setCurrentPhraseIndex] = useState(0);
    const [typing, setTyping] = useState(true);
    const [deleting, setDeleting] = useState(false);
    const [wait, setWait] = useState(false);
    const [stop, setStop] = useState(false);

    useEffect(() => {
        const intervalId = setInterval(() => {
            if (stop) {

            }
            else if (wait) {
                setWait(false);
                setDeleting(true);
            } else if (deleting) {
                setQuery((prevQuery) => prevQuery.slice(0, -1));
                if (query === '') {
                    setDeleting(false);
                    setCurrentPhraseIndex((prevIndex) => (prevIndex + 1) % phrases.length);
                    setTyping(true);
                }
            } else if (!typing && !deleting) {
                setWait(true);
            } else {
                const currentPhrase = phrases[currentPhraseIndex];
                const nextIndex = query.length + 1;

                setQuery(currentPhrase.substring(0, nextIndex));
                if (nextIndex === currentPhrase.length) {
                    setTyping(false);
                }
            }
        }, 100);

        return () => clearInterval(intervalId);
    }, [query, currentPhraseIndex, typing, deleting, wait, phrases]);

    const handleSearchClick = () => {
        setQuery('');
        setTyping(false);
        setDeleting(false);
        setWait(false);
        setStop(true);
    };
    const handleInputChange = (event) => {
        setQuery(event.target.value);
    };

    return (
        <SearchContainer onClick={handleSearchClick}>
            <SearchIcon className="fas fa-search"></SearchIcon>
            <SearchInput
                type="text"
                value={query}
                onChange={handleInputChange}
                placeholder={phrases[currentPhraseIndex]}
            />
        </SearchContainer>
    );
};

export default SearchBar;
