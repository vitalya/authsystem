import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: 100vh;
    margin-top: -80px;
    background-color: ${({ theme }) => theme.background};
    color: ${({ theme }) => theme.color};
`;

export const HeadingText = styled.h2`
    margin: 20px 0;
`;


export const InputLink = styled.a`
    position: absolute;
    right: 10px;
    top: 50%;
    transform: translateY(-50%);
    color: #AAA;
    font-size: 0.9rem;
    cursor: pointer;
    text-decoration: none;
`;


export const Form = styled.form`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 300px;
`;

export const InputGroup = styled.div`
    position: relative;
    margin: 8px 0;
`;

export const Input = styled.input`
    padding: 12px;
    border-radius: 10px;
    border-color: #597164;
    width: 100%;
`;

export const Divider = styled.div`
    color: gray;
    margin: 20px 0;
    display: flex;
    align-items: center;
    text-align: center;
    width: 100%;
    max-width: 300px;
    &::before, &::after {
        content: "";
        flex: 1;
        border-bottom: 1px solid gray;
    }
    &::before {
        margin-right: 10px;
    }
    &::after {
        margin-left: 10px;
    }
`;

export const BottomLink = styled.a`
    margin-left: 10px;
    padding: 10px 10px;
    background: ${({theme}) => theme.background};
    color: ${({theme}) => theme.color};
    border: 1px solid ${({theme}) => theme.color};
    border-radius: 10px;
    font-size: 0.9rem;
    margin-top: 10px;
    cursor: pointer;
    text-decoration: none;

    .text {
        color: ${({theme}) => theme.color};
    }

    .register {
        color: #7075EB;
    }
`;
