import React, {useEffect, useState} from 'react';
import styled from 'styled-components';
import Cookies from "js-cookie";

const CardContainer = styled.div`
    width: 300px;
    height: auto;
    margin: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
    box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.2);

    &:hover {
        transform: scale(1.02);
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.37);
    }
`;

const CardImage = styled.img`
    width: 150px;
    height: 150px;
    object-fit: cover;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
`;

const CardContent = styled.div`
    padding: 10px;
`;

const CardTitle = styled.h3`
    margin: 0;
`;

const CardDetails = styled.div`
    margin-top: 5px;
`;

const CardDifficulty = styled.span`
    margin-right: 10px;
`;

const CardTime = styled.span``;


const RecipeCard = ({imageUrl, title, difficulty, time, onClick}) => {

    const [image, setImage] = useState({});
    const getPhoto = async (id) => {
        const accessToken = Cookies.get('access_token');
        try {
            const response = await fetch(`http://127.0.0.1:8081/api/v1/storage/${id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            setImage(response.url)
        } catch (error) {
            console.error('Error during fetching photo:', error);
            return null;
        }
    };
    useEffect(() => {
        getPhoto(imageUrl);
    }, []);


    return (
        <CardContainer onClick={onClick} sx={{cursor: 'pointer', margin: 2, maxWidth: '500px'}}>
            <CardImage src={image} alt={title}/>
            <CardContent>
                <CardTitle>{title}</CardTitle>
                <CardDetails>
                    <CardDifficulty>Сложность: {difficulty}</CardDifficulty>
                    <CardTime>Время: {time} мин.</CardTime>
                </CardDetails>
            </CardContent>
        </CardContainer>
    );
};

export default RecipeCard;
