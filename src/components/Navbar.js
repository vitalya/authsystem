import React from 'react';
import styled from 'styled-components';
import {useTranslation} from 'react-i18next';
import {useTheme} from '../theme/ThemeProvider';
import {useNavigate} from "react-router-dom";
import {Box, Button} from "@mui/material";

const Nav = styled.nav`
    width: 100%;
    background-color: ${({theme}) => theme.background};
    color: ${({theme}) => theme.color};
    padding: 26px 35px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

const StyledSelect = styled.select`
    margin-left: 10px;
    padding: 5px 10px;
    background: ${({theme}) => theme.background};
    color: ${({theme}) => theme.color};
    border: 1px solid ${({theme}) => theme.color};
    border-radius: 10px;
    cursor: pointer;
    appearance: none; // Убираем стандартный стиль select
    font-size: 20px; // Устанавливаем размер шрифта
    background-image: url("${({flag}) => flag}"); // Иконка флага
    background-repeat: no-repeat;
    background-position: right 10px center;
    background-size: 20px 20px;
`;

const SvgIcon = styled.svg`
    width: 24px;
    height: 24px;
`;

export default function Navbar() {
    const {t, i18n} = useTranslation();
    const {theme, toggleTheme} = useTheme();
    const navigate = useNavigate();
    const changeLanguage = (event) => {
        i18n.changeLanguage(event.target.value);
    };

    const changeTheme = (event) => {
        toggleTheme(event.target.value);
    };

    const logoutSubmit = async (event) => {
        const accessToken = localStorage.getItem('access_token');

        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/logout', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (!response.ok) {
                throw new Error('Logout request failed');
            }

            // Удаление access token из localStorage
            localStorage.removeItem('access_token');

            // Перенаправление пользователя на страницу /login
            navigate('/login');
        } catch (error) {
            console.error('Error during logout:', error);
            // Обработка ошибки (например, показ сообщения об ошибке пользователю)
        }
    };

    const Navhome = async (event) => {
        navigate('/');

    }

    return (
        <Nav>
            <Box onClick={Navhome}
                 sx={{
                     cursor: 'pointer',
                     border: '1px solid grey',
                     borderRadius: '15px',
                     padding: 1
                 }}
            >
                {t('AppName')}
            </Box>
            <Box sx={{display: 'flex'}}>
                <StyledSelect value={theme} onChange={changeTheme}>
                    <option value="light">🌞</option>
                    <option value="dark">🌙</option>
                </StyledSelect>
                <StyledSelect onChange={changeLanguage} defaultValue={i18n.language}>

                    <option value="en">🇬🇧</option>
                    <option value="ru">🇷🇺</option>
                    <option value="es">🇪🇸</option>
                    <option value="pt">🇵🇹</option>
                </StyledSelect>
                <Button href="#" onClick={logoutSubmit} sx={{
                    marginLeft: '10px',
                    borderRadius: '10px',
                    border: '1px solid black',
                    display: 'flex',
                    justifyContent: 'center'
                }}>
                    <SvgIcon viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21 12L13 12" stroke="red" strokeWidth="2" strokeLinecap="round"
                              strokeLinejoin="round"/>
                        <path d="M18 15L20.913 12.087V12.087C20.961 12.039 20.961 11.961 20.913 11.913V11.913L18 9"
                              stroke="red" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                        <path
                            d="M16 5V4.5V4.5C16 3.67157 15.3284 3 14.5 3H5C3.89543 3 3 3.89543 3 5V19C3 20.1046 3.89543 21 5 21H14.5C15.3284 21 16 20.3284 16 19.5V19.5V19"
                            stroke="red" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                    </SvgIcon>
                </Button>
            </Box>
        </Nav>
    )
        ;
}
