import styled from "styled-components";

export const ButtonBlue = styled.button`
    background-color: #5568FE;
    color: white;
    border: none;
    padding: 12px;
    margin-top: 10px;
    border-radius: 4px;
    cursor: pointer;
    width: 100%;
`;

export const ButtonGrey = styled.button`
    background-color: #384760;
    color: white;
    border: none;
    padding: 12px;
    margin-top: 0px;
    border-radius: 4px;
    cursor: pointer;
    width: 100%;
`;
