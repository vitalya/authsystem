import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translationEN from './locales/translation_en.json';
import translationRU from './locales/translation_ru.json';
import translationES from './locales/translation_es.json';
import translationPT from './locales/translation_pt.json';

const resources = {
    en: {
        translation: translationEN
    },
    ru: {
        translation: translationRU
    },
    es: {
        translation: translationES
    },
    pt: {
        translation: translationPT
    }
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: 'ru',
        interpolation: {
            escapeValue: false,
        },
    });

export default i18n;
