import React, {useEffect, useState} from 'react';
import {MainContainer} from "../../components/MainComponents";
import {
    Box,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Typography,
    CircularProgress
} from "@mui/material";
import {useParams} from "react-router-dom";
import Cookies from "js-cookie";

export default function RecipePage() {
    const {id} = useParams();
    const [Rdata, setRData] = useState({});
    const [ingredients, setIngredients] = useState([]);
    const [steps, setSteps] = useState([]);
    const [mainimage, setImagemain] = useState('');
    const [loading, setLoading] = useState(true); // Состояние для загрузки данных рецепта
    const [loadingImage, setLoadingImage] = useState(true); // Состояние для загрузки изображения

    const getRecipe = async (id) => {
        const accessToken = Cookies.get('access_token');
        try {
            const response = await fetch(`http://127.0.0.1:8081/api/v1/recipe/${id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            console.log(data);
            setRData(data.recipe);
            setIngredients(data.ingredients);
            setSteps(data.steps);
        } catch (error) {
            console.error('Error during fetching recipe:', error);
        } finally {
            setLoading(false); // Завершаем загрузку данных рецепта
        }
    };

    const getPhoto = async (id) => {
        const accessToken = Cookies.get('access_token');
        try {
            const response = await fetch(`http://127.0.0.1:8081/api/v1/storage/${id}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${accessToken}`
                }
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.url;
        } catch (error) {
            console.error('Error during fetching photo:', error);
            return null;
        } finally {
            setLoadingImage(false); // Завершаем загрузку изображения
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true); // Начинаем загрузку данных рецепта
            await getRecipe(id);
        };

        fetchData();
    }, [id]);

    useEffect(() => {
        if (Rdata.photoId) {
            const fetchPhoto = async () => {
                setLoadingImage(true); // Начинаем загрузку изображения
                const url = await getPhoto(Rdata.photoId);
                setImagemain(url);
            };

            fetchPhoto();
        } else {
            setLoadingImage(false); // Нет изображения, завершаем загрузку
        }
    }, [Rdata]);

    if (loading) {
        return (
            <MainContainer>
                <Box sx={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
                    <CircularProgress/>
                </Box>
            </MainContainer>
        );
    }

    return (
        <MainContainer>
            <Box
                sx={{
                    maxWidth: '800px',
                    margin: '0 auto',
                    padding: '16px',
                }}>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center'
                }}>
                    <Typography variant="h3" sx={{marginY: 2, fontWeight: '600',}}>{Rdata.name}</Typography>
                    {loadingImage ? (
                        <CircularProgress/>
                    ) : (
                        <img src={mainimage}
                             alt="Описание изображения"
                             style={{width: '50%', height: '50%'}}/>
                    )}
                    <Typography sx={{marginY: 2,}}>{Rdata.description}</Typography>
                    <Typography variant="h5" sx={{marginY: 2, fontWeight: '500',}}>ИНГРЕДИЕНТЫ</Typography>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Ингредиент</TableCell>
                                    <TableCell>Количество</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {ingredients.map((ingredient, index) => (
                                    <TableRow key={index}>
                                        <TableCell>{ingredient.name}</TableCell>
                                        <TableCell>{ingredient.count}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Typography variant="h3" sx={{marginY: 2, fontWeight: '600',}}>
                        Пошаговый рецепт с фото
                    </Typography>
                    <Box>
                        {steps.map((step, index) => (
                            <Box key={step.id} sx={{marginY: 2}}>
                                <Typography variant="h6" sx={{marginY: 2, fontWeight: '600'}}>
                                    Шаг {index + 1}
                                </Typography>
                                <Typography variant="body1" sx={{marginY: 2, fontWeight: '500', fontSize: '18px'}}>
                                    {step.description}
                                </Typography>
                            </Box>
                        ))}
                    </Box>
                </Box>
            </Box>
        </MainContainer>
    );
}
