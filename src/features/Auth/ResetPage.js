import React, { useState } from 'react';
import styled from 'styled-components';
import '@fortawesome/fontawesome-free/css/all.css';

import { useTranslation } from 'react-i18next';
import {
    BottomLink,
    Container,
    Divider,
    HeadingText,
    Input,
    InputGroup,
    InputLink,
    Form
} from "../../components/AuthComponents";
import { ButtonBlue, ButtonGrey } from "../../components/Buttons";
import { useNavigate } from "react-router-dom";

export default function ResetPage() {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [smsCode, setSmsCode] = useState('');
    const [showSMSField, setShowSMSField] = useState(false);


    const handleSMSCodeChange = (event) => {
        setSmsCode(event.target.value);
    };

    const loginByPhoneSubmit = (event) => {
        event.preventDefault();
        if (showSMSField) {
            // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
            navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
        } else {
            setShowSMSField(true);
        }
    };

    const loginSubmit = (event) => {
        event.preventDefault();
        navigate('/login');
    };

    const registerSubmit = (event) => {
        event.preventDefault();
        navigate('/register');
    };

    return (
        <Container>
            <i className="fas fa-user-circle" style={{ fontSize: '48px' }}></i>

            <HeadingText>{t('Make yourself at home!')}</HeadingText>
            <Form onSubmit={loginByPhoneSubmit}>
                <InputGroup>
                    <Input type="text" placeholder={t('Phone number')}  />
                </InputGroup>
                {showSMSField && (
                    <InputGroup>
                        <Input type="text" placeholder={t('SMS code')} value={smsCode} onChange={handleSMSCodeChange} />
                    </InputGroup>
                )}
                <ButtonBlue type="submit">{!showSMSField ? t('Continue') : t('Auth')}</ButtonBlue>
            </Form>

            <Divider>{t('or')}</Divider>

            <Form onSubmit={loginSubmit}>
                <ButtonGrey type="submit" sx={{ background: 0 }}>{t('Auth by password')}</ButtonGrey>
            </Form>


            <BottomLink href="#">
                <span className="text">{t('No account?')} </span>
                <span className="register" onClick={registerSubmit}>{t('Register')}</span>
            </BottomLink>
        </Container>
    );
}
