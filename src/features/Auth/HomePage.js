import React, {useEffect, useState} from 'react';
import '@fortawesome/fontawesome-free/css/all.css';

import {t} from "i18next";
import SearchBar from "../../components/SearchBar";
import {MainContainer} from "../../components/MainComponents"; // Предположим, что MainContainer импортируется из этого пути
import RecipeCard from "../../components/RecipeCard";
import {Box} from "@mui/material";
import {useNavigate} from "react-router-dom";
import Cookies from "js-cookie";

// Тестовые данные для рецептов
const recipes = [
    {
        id: 1,
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu2-TryntzeTWLW3slWN-DIGu_VBbS8DRXZ00wCdmbvQ&s',
        title: 'Пирог',
        difficulty: 'Easy',
        time: 30,
    },
    {
        id: 2,
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu2-TryntzeTWLW3slWN-DIGu_VBbS8DRXZ00wCdmbvQ&s',
        title: 'Спагетти',
        difficulty: 'Easy',
        time: 30,
    },
    {
        id: 3,
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTu2-TryntzeTWLW3slWN-DIGu_VBbS8DRXZ00wCdmbvQ&s',
        title: 'Какая то шляпа',
        difficulty: 'Easy',
        time: 30,
    },
];

export default function HomePage() {

    const [recipes, setRecipes] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        const getRecipies = async () => {
            const accessToken = Cookies.get('access_token');
            try {
                const response = await fetch('http://127.0.0.1:8081/api/v1/recipe/', {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${accessToken}`
                    }
                });

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                const data = await response.json();
                // Обновляем состояние с загруженными рецептами
                setRecipes(data.answer);


            } catch (error) {
                console.error('Error during fetching recipes:', error);
                // Обработка ошибки загрузки (например, показать сообщение об ошибке пользователю)
            }
        };

        getRecipies();
    }, []);

    const handleCardClick = (id) => {
        navigate(`/recipe/${id}`);
    };
    return (
        <MainContainer>

            {/*
            <SearchBar
                phrases={[t('First placeholder'), t('Second placeholder'), t('Third placeholder')]}
            /> */}

            <Box sx={{
                display: 'flex',
                maxWidth: '50vw',
                flexWrap: 'wrap',
                flexDirection: {xs: 'column', sm: 'row', md: 'row'},
                justifyContent: {xs: 'center', sm: 'center', md: 'center'},
                alignItems: {xs: 'center', sm: 'center', md: 'center'}
            }}>
                {recipes.map((recipe, index) => (
                    <RecipeCard
                        key={index}
                        imageUrl={recipe.photoId}
                        title={recipe.name}
                        difficulty={recipe.cookDifficult}
                        time={recipe.summaryCookTimeMinutes}
                        onClick={() => handleCardClick(recipe.id)}
                    />
                ))}
            </Box>
        </MainContainer>
    );
}
