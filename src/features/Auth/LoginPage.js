import React, {useState} from 'react';
import '@fortawesome/fontawesome-free/css/all.css';
import {useTranslation} from 'react-i18next';
import Cookies from 'js-cookie';
import {
    BottomLink,
    Container,
    Divider,
    HeadingText,
    Input,
    InputGroup,
    InputLink,
    Form
} from "../../components/AuthComponents";
import {ButtonBlue, ButtonGrey} from "../../components/Buttons";
import {useNavigate} from "react-router-dom";

export default function LoginPage() {
    const {t} = useTranslation();
    const navigate = useNavigate();

    const loginByPhoneSubmit = (event) => {
        event.preventDefault();
        navigate('/login-by-phone');
    };

    const registerSubmit = (event) => {
        event.preventDefault();
        navigate('/register');
    }

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const loginSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username,
                    password,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            Cookies.set('access_token', data.access_token, {expires: 7}); // Сохраняем токен на 7 дней
            // Process the response data as needed

            // Navigate to the home page upon successful login
            navigate('/home');
        } catch (error) {
            console.error('Error during login:', error);
            // Handle login error (e.g., show an error message to the user)
        }
    };

    return (
        <Container>
            <i className="fas fa-user-circle" style={{fontSize: '48px'}}></i>

            <HeadingText>{t('Welcome back!')}</HeadingText>
            <Form>

                <InputGroup>
                    <Input type="text" placeholder={t('Auth or email')}
                           value={username}
                           onChange={(e) => setUsername(e.target.value)}
                    />
                </InputGroup>
                <InputGroup>
                    <Input type="password" placeholder={t('Password')} value={password}
                           onChange={(e) => setPassword(e.target.value)}/>
                    <InputLink href="#">{t('Forgot password?')}</InputLink>
                </InputGroup>

                <ButtonBlue type="submit" onClick={loginSubmit}>{t('Continue')}</ButtonBlue>
            </Form>

            <Divider>{t('or')}</Divider>
            <Form onSubmit={loginByPhoneSubmit}>
                <ButtonGrey type="submit" sx={{background: 0}}>{t('Auth by phone')}</ButtonGrey>

            </Form>
            <BottomLink href="#">
                <span className="text">{t('No account?')} </span>
                <span className="register" onClick={registerSubmit}>{t('Register')}</span>
            </BottomLink>
        </Container>
    );
}
