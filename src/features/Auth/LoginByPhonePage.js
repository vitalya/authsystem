import React, {useState} from 'react';
import '@fortawesome/fontawesome-free/css/all.css';

import {useTranslation} from 'react-i18next';
import {
    BottomLink,
    Container,
    Divider,
    HeadingText,
    Input,
    InputGroup,
    Form
} from "../../components/AuthComponents";
import {ButtonBlue, ButtonGrey} from "../../components/Buttons";
import {useNavigate} from "react-router-dom";
import Cookies from "js-cookie";

export default function LoginByPhonePage() {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const [smsCode, setSmsCode] = useState('');
    const [phone, setPhone] = useState('');
    const [id, setId] = useState('');
    const [showSMSField, setShowSMSField] = useState(false);


    const phoneSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/send-sms', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    phone,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            setId(data.code_id)

            // Process the response data as needed
            console.log(data);

            // Navigate to the home page upon successful login

        } catch (error) {
            console.error('Error during login:', error);
            // Handle login error (e.g., show an error message to the user)
        }
        if (showSMSField) {
            navigate('/home');
        } else {
            setShowSMSField(true);
        }
    };

    const loginphoneSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/login-by-phone', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "phone": phone,
                    "sms": smsCode,
                    "codeId": id,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            // Process the response data as needed
            console.log(data);
            Cookies.set('access_token', data.access_token, {expires: 7}); // Сохраняем токен на 7 дней

            // Navigate to the home page upon successful login
            if (showSMSField) {
                // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
                navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
            } else {
                setShowSMSField(true);
            }
        } catch (error) {
            console.error('Error during login:', error);
            // Handle login error (e.g., show an error message to the user)
        }
    };


    const handleSMSCodeChange = (event) => {
        setSmsCode(event.target.value);
    };

    const loginByPhoneSubmit = (event) => {
        event.preventDefault();
        if (showSMSField) {
            // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
            navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
        } else {
            setShowSMSField(true);
        }
    };

    const loginSubmit = (event) => {
        event.preventDefault();
        navigate('/login');
    };

    const registerSubmit = (event) => {
        event.preventDefault();
        navigate('/register');
    };

    return (
        <Container>
            <i className="fas fa-user-circle" style={{fontSize: '48px'}}></i>

            <HeadingText>{t('Make yourself at home!')}</HeadingText>
            <Form onSubmit={loginByPhoneSubmit}>
                <InputGroup>
                    <Input type="text" disabled={showSMSField} value={phone} placeholder={t('Phone')}
                           onChange={(e) => setPhone(e.target.value)}/>
                </InputGroup>
                {showSMSField && (
                    <InputGroup>
                        <Input type="text" placeholder={t('SMS code')} value={smsCode} onChange={handleSMSCodeChange}/>
                    </InputGroup>
                )}
                <ButtonBlue onClick={!showSMSField ? phoneSubmit : loginphoneSubmit}
                            type="submit">{!showSMSField ? t('Continue') : t('Auth')}</ButtonBlue>
            </Form>

            <Divider>{t('or')}</Divider>

            <Form onSubmit={loginSubmit}>
                <ButtonGrey type="submit" sx={{background: 0}}>{t('Auth by password')}</ButtonGrey>
            </Form>


            <BottomLink href="#">
                <span className="text">{t('No account?')} </span>
                <span className="register" onClick={registerSubmit}>{t('Register')}</span>
            </BottomLink>
        </Container>
    );
}
