import React, {useState} from 'react';
import '@fortawesome/fontawesome-free/css/all.css';

import {useTranslation} from 'react-i18next';
import {
    BottomLink,
    Container,
    Divider,
    HeadingText,
    Input,
    InputGroup,
    Form
} from "../../components/AuthComponents";
import {ButtonBlue, ButtonGrey} from "../../components/Buttons";
import {useNavigate} from "react-router-dom";
import Cookies from "js-cookie";

export default function RegisterPage() {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const [smsCode, setSmsCode] = useState('');
    const [phone, setPhone] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [id, setId] = useState('');
    const [showSMSField, setShowSMSField] = useState(false);


    const goLogin = () => {
        navigate('/login');


    }

    const registgerSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/send-sms', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    phone,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            setId(data.code_id)

            // Process the response data as needed
            console.log(data);

            // Navigate to the home page upon successful login

        } catch (error) {
            console.error('Error during login:', error);
            // Handle login error (e.g., show an error message to the user)
        }
        if (showSMSField) {
            // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
            navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
        } else {
            setShowSMSField(true);
        }
    };


    const registerSubmitSec = async (event) => {
        event.preventDefault();
        try {
            const response = await fetch('http://127.0.0.1:8081/api/v1/auth/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "username": username,
                    "email": email,
                    "phone": phone,
                    "password": password,
                    "sms": smsCode,
                    "codeId": id,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const data = await response.json();
            // Process the response data as needed
            Cookies.set('access_token', data.access_token, {expires: 7}); // Сохраняем токен на 7 дней
            console.log(data);

            // Navigate to the home page upon successful login
            if (showSMSField) {
                // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
                navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
            } else {
                setShowSMSField(true);
            }
        } catch (error) {
            console.error('Error during login:', error);
            // Handle login error (e.g., show an error message to the user)
        }
    };


    const loginByPhoneSubmit = (event) => {
        event.preventDefault();
        if (showSMSField) {
            // Здесь можно добавить проверку на правильность введенного SMS-кода и выполнить необходимые действия для входа
            navigate('/home'); // Замени это на путь куда нужно перейти после успешного входа
        } else {
            setShowSMSField(true);
        }
    };

    const loginSubmit = (event) => {
        event.preventDefault();
        navigate('/login');
    };

    return (
        <Container>
            <i className="fas fa-user-circle" style={{fontSize: '48px'}}></i>

            <HeadingText>{t('Make yourself at home!')}</HeadingText>
            <Form onSubmit={loginByPhoneSubmit}>
                <InputGroup>
                    <Input type="text" disabled={showSMSField} value={phone} placeholder={t('Phone')}
                           onChange={(e) => setPhone(e.target.value)}/>
                </InputGroup>


                {showSMSField && (
                    <InputGroup>
                        <Input sx={{mb: 10}} type="text" placeholder={t('Username')} value={username}
                               onChange={(e) => setUsername(e.target.value)}/>
                    </InputGroup>
                )}

                {showSMSField && (
                    <InputGroup>

                        <Input sx={{mb: 10}} type="text" placeholder={t('Email')} value={email}
                               onChange={(e) => setEmail(e.target.value)}/>


                    </InputGroup>
                )}

                {showSMSField && (
                    <InputGroup>

                        <Input sx={{mb: 10}} type="password" placeholder={t('Password')} value={password}
                               onChange={(e) => setPassword(e.target.value)}/>


                    </InputGroup>
                )}

                {showSMSField && (
                    <InputGroup>
                        <Input type="text" placeholder={t('SMS code')} value={smsCode}
                               onChange={(e) => setSmsCode(e.target.value)}/>
                    </InputGroup>
                )}
                <ButtonBlue
                    onClick={!showSMSField ? registgerSubmit : registerSubmitSec}
                    type="submit"
                >
                    {!showSMSField ? t('Continue') : t('Auth')}
                </ButtonBlue>
            </Form>

            <Divider>{t('or')}</Divider>

            <Form onSubmit={loginSubmit}>
                <ButtonGrey type="submit" sx={{background: 0}}>{t('Auth by password')}</ButtonGrey>
            </Form>


            <BottomLink href="#">
                <span className="text">{t('Есть аккаунт?')} </span>
                <span className="register" onClick={goLogin}>{t('Login')}</span>
            </BottomLink>
        </Container>
    );
}
