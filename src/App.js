import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {ThemeProvider} from './theme/ThemeProvider';
import './i18n';
import LoginPage from "./features/Auth/LoginPage";
import Navbar from "./components/Navbar";
import LoginByPhonePage from "./features/Auth/LoginByPhonePage";
import HomePage from "./features/Auth/HomePage";
import RegisterPage from "./features/Auth/RegisterPage";
import ResetPage from "./features/Auth/ResetPage";
import RecipePage from "./features/Recipe/RecipePage";

function App() {
    return (
        <Router>
            <ThemeProvider>
                <Navbar/>

                <Routes>
                    <Route path="/login" element={<LoginPage/>}/>
                    <Route path="/" element={<HomePage/>}/>
                    <Route path="/login-by-phone" element={<LoginByPhonePage/>}/>
                    <Route path="/home" element={<HomePage/>}/>
                    <Route path="/recipe/:id" element={<RecipePage />} />
                    <Route path="/register" element={<RegisterPage/>}/>
                    <Route path="/reset" element={<ResetPage/>}/>
                </Routes>
            </ThemeProvider>
        </Router>
    );
}

export default App;
